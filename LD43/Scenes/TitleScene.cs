﻿using Microsoft.Xna.Framework;
using Nez;
using Nez.UI;
using Nez.UI.Widgets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LD43.Input;

namespace LD43.Scenes
{
    public class TitleScene : Scene
    {
        Entity entity;
        UICanvas canvas;
        InputHandler input;

        //Title
        EffectLabel title;
        EffectLabel pressStart;

        bool transitioning = false;
        public override void initialize()
        {
            clearColor = new Color(71, 45, 60);
            var renderer = addRenderer(new DefaultRenderer());


            entity = addEntity(new Entity());
            canvas = entity.addComponent(new UICanvas());
            canvas.setRenderLayer(-10);

            var style = new LabelStyle(new Color(223, 246, 245));
            title = new EffectLabel("Ludum Dare 43", style)
                .setAlignment(Align.center)
                .setFontScale(3f);
            pressStart = new EffectLabel("/2Press 'Z' or 'J' to Start/2", style)
                .setAlignment(Align.center);
            canvas
                .stage
                .addElement(title)
                .setX(Engine.designWidth / 2)
                .setY(Engine.designHeight * 1 / 4);
            canvas
                .stage
                .addElement(pressStart)
                .setX(Engine.designWidth / 2)
                .setY(Engine.designHeight * 3 / 4);

            input = new InputHandler(0);
            input.SetupInput();
        }

        public override void update()
        {
            base.update();
            if (input.Button1Input.isPressed && !transitioning)
            {
                //transitioning = true;
                //Core.scene = new GameScene();
            }
        }
    }
}
