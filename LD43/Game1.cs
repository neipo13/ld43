﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Nez;
using Nez.Systems;
using System.Collections.Generic;

namespace LD43
{
    public enum GameEvents
    {
        GameStart,
        GameOver
    }

    public struct GameEventsComparer : IEqualityComparer<GameEvents>
    {
        public bool Equals(GameEvents x, GameEvents y)
        {
            return x == y;
        }


        public int GetHashCode(GameEvents obj)
        {
            return (int)obj;
        }
    }
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Engine : Core
    {
        public static int designWidth = 256 * 2;
        public static int designHeight = 144 * 2;
        public static Emitter<GameEvents> gameEventEmitter = new Emitter<GameEvents>(new GameEventsComparer());

        public Engine() : base(windowTitle: "LD43", isFullScreen: false)
        {
            var policy = Scene.SceneResolutionPolicy.BestFit;
            Scene.setDefaultDesignResolution(designWidth, designHeight, policy, 0, 0);
            Window.AllowUserResizing = true;

            Nez.Input.maxSupportedGamePads = 1;
        }
        protected override void Initialize()
        {
            base.Initialize();
            scene = new Scenes.TitleScene();
        }
    }
}
